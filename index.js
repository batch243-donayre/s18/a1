
	
	/*1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.*/
	
	function sum(addendOne, addendtwo) {
		console.log("Displayed sum of " +addendOne+ " and " +addendtwo);
		let isSumOf = addendOne + addendtwo;
		console.log(isSumOf);
	}
		sum(5,15);



		/*Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function*/

	function difference(minuend, subtrahend) {
		console.log("Displayed difference of " +minuend+ " and " +subtrahend);
		let isDifferenceOf = minuend - subtrahend;
		console.log(isDifferenceOf);
	}
		difference(20 , 5);




	/*2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.*/

	let isProductOf;

	function product(multiplicand, multiplier) {
		console.log("Displayed product of " +multiplicand+ " and " +multiplier);

		return multiplicand * multiplier;
	}
		isProductOf = product(50, 10);
		console.log(isProductOf);


		/*Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.*/


	let isQuotientOf;
	function quotient(dividend, divisor) {
		console.log("Displayed quotient of " +dividend+ " and " +divisor);

		return isQuotientOf = dividend / divisor;

	}
		isQuotientOf = quotient(50, 10);
		console.log(isQuotientOf)



	 	/*Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.*/
		/*Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.*/

		/*Log the value of product variable in the console.
		Log the value of quotient variable in the console.*/

	/*3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.
*/


	let circleArea;

	function areaOfCircle(radius){
		console.log("The result of getting the area of a circle with "+radius+ " radius:");

		return Math.PI * (radius ** 2);
	}

	circleArea = areaOfCircle(15);
	console.log (circleArea.toFixed(2));


	/*4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/

	let averageVar;

	function average(numOne, numTwo, numThree, numFour){
		console.log("The avarage of "+numOne+ "," +numTwo+ "," +numThree+ "," +numFour+ " :");

		return averageVar = (numOne+numTwo+numThree+numFour)/4;
		
	}
		averageVar = average(20, 40, 60, 80);
		console.log(averageVar);	

	/*5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.*/

	let isPassingScore;

	function passingScore(score,overallTotal){
		console.log("Is "+score+ "/" +overallTotal+ " a passing score?" );
		return isPassingScore = (score/overallTotal) *100 >= 75;
	}

	isPassingScore = passingScore(38,50);
	console.log(isPassingScore);